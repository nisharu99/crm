<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            "email" => "required|email",
            "password" => "required"
        ]);

        $user = UserModel::where('email', '=', $request->input('email'))->first();
        // return $user;
        // if user
        if ($user) {
            //check entered password
            if (Hash::check($request->input('password'), $user->password)) {

                // here auth login
                Auth::login($user);

                return redirect()->route('companies.index');
            } else {
                //invalid user
                return view('auth.login')->with('fail', 'Invalid email or password!');
            }
        }
    }

    public function logout()
    {
        //logout
        Auth::logout();
        if (!Auth::check()) {
            return view('auth.login');
        }
    }

}
