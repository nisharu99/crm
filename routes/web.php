<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return view('auth.login');
})->name('login');

Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');


Route::group(['middleware'=>['auth']],function(){
        // Companies Routes
Route::get('/companies/index', [CompanyController::class, 'index'])->name('companies.index');
Route::get('/companies/create', [CompanyController::class, 'create'])->name('companies.create');
Route::post('/companies/store',[CompanyController::class,'store'])->name('companies.store');
Route::get('/companies/edit/{id}',[CompanyController::class,'edit'])->name('companies.edit');
Route::post('/companies/update/{id}',[CompanyController::class,'update'])->name('companies.update');
Route::delete('/companies/delete/{id}',[CompanyController::class,'destroy'])->name('companies.delete');


        // Employees Routes
    Route::get('/employees/index', [EmployeeController::class, 'index'])->name('employees.index');
    Route::get('/employees/create', [EmployeeController::class, 'create'])->name('employees.create');
    Route::post('/employees/store',[EmployeeController::class,'store'])->name('employees.store');
    Route::get('/employees/edit/{id}',[EmployeeController::class,'edit'])->name('employees.edit');
    Route::post('/employees/update/{id}',[EmployeeController::class,'update'])->name('employees.update');
    Route::delete('/employees/delete/{id}',[EmployeeController::class,'destroy'])->name('employees.delete');
    // Route::post('/employees/fetchByStatus', [EmployeeController::class, 'fetchByStatus'])->name('employees.fetchByStatus');
});
