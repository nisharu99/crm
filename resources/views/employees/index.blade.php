@extends('layouts.master')
{{-- <script>
    $(document).ready(function() {
        $('#status-switch').on('change', function() {
            var status = $(this).prop('checked') ? 'active' : 'inactive';
            fetchEmployees(status);
        });

        function fetchEmployees(status) {
            $.ajax({
                url: '{{ route('employees.fetchByStatus') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    status: status
                },
                success: function(response) {
                    $('#employee-table-body').html(response);
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
    });
</script> --}}
<div class="container">
    @if (session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <h2>Employees</h2>

    <a href="{{ route('employees.create') }}" class="btn btn-primary mb-3">Create Employees</a>
    <a href="{{ route('companies.index') }}" class="btn btn-success mb-3">Companies</a>
    <div class="d-flex justify-content-end">
        <a href="{{ route('logout') }}" class="btn btn-danger mb-3 ">Logout</a>
    </div>

    {{-- <div class="form-group">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="status-switch">
            <label class="custom-control-label" for="status-switch">Show inactive Employees</label>
        </div>
    </div> --}}

    <table class="table">
        <thead>
            <tr>
                <th>Company Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $emp)
                <tr>
                    <td>{{ $emp->company->name }}</td>
                    <td>{{ $emp->first_name }}</td>
                    <td>{{ $emp->last_name }}</td>
                    <td>{{ $emp->email }}</td>
                    <td>{{ $emp->phone }} </td>
                    <td>
                        <span class="badge {{ $emp->status ? 'badge-success' : 'badge-danger' }}">
                            {{ $emp->status ? 'Active' : 'Inactive' }}
                        </span>
                    </td>
                    <td>
                        <a href="{{ route('employees.edit', $emp->id) }}" class="btn btn-sm btn-primary">Edit</a>
                        <form action="{{ route('employees.delete', $emp->id) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger"
                                onclick="return confirm('Are you sure you want to delete this company?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $employees->links() }}
